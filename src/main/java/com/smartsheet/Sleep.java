package com.smartsheet;

/**
 * Mostly a wrapper for Thread.sleep that eats the exception, to make it easier.
 *
 *
 * This should go away. And be a replaced by a polling method that waits
 * for a change in state.
 * eg wait for 60 seconds until passed in function returns true; Or raise exception
 */
public class Sleep {

    public static void sleep(int seconds) {
        try {
            Thread.sleep(1000 * seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}

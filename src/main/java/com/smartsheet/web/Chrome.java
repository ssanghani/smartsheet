package com.smartsheet.web;


import com.smartsheet.TestProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Browser & Chrome need to be redesigned, see Readme.txt
 */
public class Chrome implements Browser {
    private final WebDriver driver;
    public Chrome() {
        TestProperties prop = TestProperties.INSTANCE;
        System.setProperty("webdriver.chrome.driver", prop.getChromeDriverLocation());
        driver = new ChromeDriver();
    }
    @Override
    public WebDriver getWebDriver() {
        return driver;
    }

}

package com.smartsheet.web;

import org.openqa.selenium.WebDriver;

interface Browser {

    WebDriver getWebDriver();
}

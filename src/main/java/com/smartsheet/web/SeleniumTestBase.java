package com.smartsheet.web;

import com.smartsheet.Sleep;
import com.smartsheet.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.smartsheet.Sleep.sleep;

/**
 * Abstract Base class for all(?) Selenium tests
 */
public abstract class SeleniumTestBase extends TestBase {

    /**
     * Get us past the login page, this is common enough to put here.
     */
    public void login() {

        String link = properties.getWebEndPoint();
        WebDriver browser = getBrowser();
        browser.get(link);
        // Get the WebElement corresponding to the Email Address(TextField)
        WebElement email = browser.findElement( By.id("loginEmail"));
        email.sendKeys(properties.getWebLogin());
        WebElement login = browser.findElement(By.id("formControl"));
        login.click();

        Sleep.sleep(5);
        // Get the WebElement corresponding to the Password Field
        WebElement password = browser.findElement(By.name("loginPassword"));
        password.sendKeys(properties.getWebPassword());
        login = browser.findElement(By.id("formControl"));
        login.click();
        sleep(5); //gremlins


    }

    public abstract WebDriver getBrowser();

    /**
     * make sure browser is closed to be a good citizen
     */
    protected void cleanup() {
        getBrowser().close();
    }
}

package com.smartsheet;

/**
 * Base class for all tests
 */
public class TestBase {
    protected TestProperties properties;

    protected TestBase() {
        properties = TestProperties.INSTANCE;
    }

}

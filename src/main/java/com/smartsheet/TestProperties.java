package com.smartsheet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Reads the properties file. This is set to be a Singleton
 */
public enum TestProperties {
    INSTANCE;
    private final Properties prop;
    TestProperties() {
        prop = new Properties();
        try {
            InputStream input = new FileInputStream("config/my.properties");
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String get(String key) {
        String s = prop.getProperty(key);
        if (s == null) throw new UnsupportedOperationException("Key does not exist");
        return s;
    }

    public String getChromeDriverLocation() {
        return get("chromeDriver");
    }
    public String getApiKey() {
        return "Bearer " + get("apiKey");
    }

    // TODO - future no need to write separate methods, use reflection
    public String getApiEndpoint() {
        return get("apiEndPoint");
    }

    public String getWebLogin() {
        return get("webLogin");
    }


    public String getWebPassword() {
        return get("webPassword");
    }

    public String getWebEndPoint() {
        return get("webEndPoint");
    }
}


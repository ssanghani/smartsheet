package com.smartsheet.api;

import com.google.gson.Gson;
import com.smartsheet.SpecFactory;
import com.smartsheet.json.ColumnCreateRequest;
import com.smartsheet.json.SheetCreateRequest;
import com.smartsheet.json.SheetCreateResponse;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang.RandomStringUtils;

import static io.restassured.RestAssured.given;

/**
 * This is the low level class that interfaces with the Sheets API.
 * If a create is issued and is successful, it will save off the response for further processing
 *
 * It does not use Strings for Request or Response but Objects that are serialized/deserialized
 * as needed.
 */
public class Sheet {
    private boolean isCreated;
    private SheetCreateResponse response;

    /**
     * Create a string with some decent defaults
     * @return
     *  ValidatableResponse (easier for testing, can use asserts directly)
     */
    public ValidatableResponse create() {
        ColumnCreateRequest c = new ColumnCreateRequest(true,
                RandomStringUtils.randomAlphanumeric(8), "TEXT_NUMBER");

        SheetCreateRequest s = new SheetCreateRequest(RandomStringUtils.randomAlphanumeric(8));
        s.addColumn(c);
        return create(s, SpecFactory.getApiSpec());
    }

    /**
     * Create a Sheet given the request object
     * @param sheetCreate
     *      request object
     * @return
     *      ValidatableResponse
     */
    public ValidatableResponse create(SheetCreateRequest sheetCreate) {
        return create(sheetCreate, SpecFactory.getApiSpec());
    }

    /**
     * Create a sheet given the request object & a RequestSpec. This saves off the
     * response object if the response code is 200.
     * @param sheetCreate
     *          request object
     * @param spec
     *      RequestSpecification (this contains apiKey and URI)
     * @return
     *      ValidatablResponse
     */
    public ValidatableResponse create(SheetCreateRequest sheetCreate, RequestSpecification spec) {
        if (isCreated) throw new RuntimeException("Your sheet is already created");
        String body = new Gson().toJson(sheetCreate);
        ValidatableResponse vr =  given()
                .spec(spec)
                .body(body)
                .post().then();

        if(vr.extract().statusCode() == 200) {
            extractResponse(vr);
        }

        return vr;
    }

    /**
     * Delete the sheet. Throw an exception if the sheet was never created.
     * That should get changed to check if the sheet exists.
     *
     * @return
     *  ValidatableResponse
     */
    public ValidatableResponse delete() {
        if (!isCreated) throw new RuntimeException("Your sheet is not created");
        return given()
                .spec(SpecFactory.getApiSpec())
                .basePath(String.valueOf(response.getResult().getId()))
                .delete().then();
    }

    /**
     * return the saved response
     * @return
     * SheetCreateResponse
     */
    public SheetCreateResponse getResponse() {
        if(!isCreated) throw new RuntimeException("Your sheet is not created");
        return response;
    }

    //internal deserialize response
    private void extractResponse(ValidatableResponse vr) {
        response = new Gson().fromJson(vr.extract().body().asString(),
                SheetCreateResponse.class);
        isCreated = true;
    }
}

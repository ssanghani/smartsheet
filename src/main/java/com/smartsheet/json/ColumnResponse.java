package com.smartsheet.json;

import lombok.Data;

/**
 * Represent the JSON Column part of Sheet create response
 */
public @Data
class ColumnResponse {
     String id;
     String index;
     String symbol;
     String title;
     String type;
}

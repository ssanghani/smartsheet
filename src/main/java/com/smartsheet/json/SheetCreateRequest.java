package com.smartsheet.json;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent JSON of a Sheet Create Request
 */
public @Data class SheetCreateRequest {

    String name;
    List<ColumnCreateRequest> columns;

    public SheetCreateRequest(String name) {
        this.name = name;
        columns = new ArrayList<>();
    }

    public void addColumn(ColumnCreateRequest c){
        columns.add(c);
    }
}

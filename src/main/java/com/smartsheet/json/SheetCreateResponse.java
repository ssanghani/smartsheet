package com.smartsheet.json;

import lombok.Data;

/**
 * JSON representation of the Sheet create response
 */
public @Data class SheetCreateResponse {
    String message;
    int resultCode;
    SheetResultResponse result;

}

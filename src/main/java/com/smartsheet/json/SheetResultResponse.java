package com.smartsheet.json;

import lombok.Data;

import java.util.List;

/**
 * JSON representation of the 'Result' part in Sheet create response
 */
public @Data class SheetResultResponse {
    String accessLevel;
    long id;
    String name;
    String permalink;
    List<ColumnResponse> columns;
}

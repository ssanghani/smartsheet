package com.smartsheet.json;

import lombok.Data;
import org.apache.commons.lang.RandomStringUtils;

/**
 * Represent the JSON for the Column in the sheet create request
 */
public @Data class ColumnCreateRequest {
    boolean primary;
    String title;
    String type;
    boolean hidden;
    String format;
    String systemColumnType;
    int width;

    public ColumnCreateRequest(boolean primary, String title, String type) {
        this.primary = primary;
        this.title = title;
        this.type = type;
    }

    public ColumnCreateRequest() {
        this.primary = true;
        this.title = RandomStringUtils.randomAlphanumeric(5);
        this.type = "TEXT_NUMBER";
    }
}

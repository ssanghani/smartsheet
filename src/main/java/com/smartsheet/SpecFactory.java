package com.smartsheet;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

/**
 * Beginnings of a factory to talk to all services.
 * This can be extended to try different auth methods and even talk to internal
 * services.
 */
public class SpecFactory {

    public static RequestSpecification getApiSpec() {
        return getApiSpecBuilder().build();
    }

    private static RequestSpecBuilder getApiSpecBuilder() {
        TestProperties prop = TestProperties.INSTANCE;
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addHeader("Authorization", prop.getApiKey());
        builder.setContentType("application/json");
        builder.setBaseUri(prop.getApiEndpoint());
        return builder;
    }
}

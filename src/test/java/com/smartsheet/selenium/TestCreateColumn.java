package com.smartsheet.selenium;

import com.smartsheet.api.Sheet;
import com.smartsheet.json.ColumnCreateRequest;
import com.smartsheet.json.SheetCreateRequest;
import com.smartsheet.web.Chrome;
import com.smartsheet.web.SeleniumTestBase;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.List;

import static com.smartsheet.Sleep.sleep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


public class TestCreateColumn extends SeleniumTestBase {
    private final WebDriver browser; // this should come via dataProvider for different browsers
    private final List<Sheet> sheets;

    public TestCreateColumn() {
        super();
        browser = new Chrome().getWebDriver();
        sheets = new ArrayList<>();
    }

    @Override
    public WebDriver getBrowser() {
        return browser;
    }

    // just create a sheet using the API
    private Sheet getSheet() {
        Sheet sheet = new Sheet();
        SheetCreateRequest sr = new SheetCreateRequest(RandomStringUtils.randomAlphanumeric(5));
        ColumnCreateRequest c = new ColumnCreateRequest();
        sr.addColumn(c);
        sheet.create(sr);

        sheets.add(sheet);
        return sheet;
    }
    @BeforeClass
    public void setup() {
        login();
    }

    @AfterClass
    public void cleanup() {
        super.cleanup();
        sheets.forEach(Sheet::delete);
    }

    // create a sheet using API. Point the browser to that sheet.
    private void initASheet() {
        String link = getSheet().getResponse().getResult().getPermalink();
        browser.get(link);
        sleep(2);
    }
    @DataProvider
    public Object[][] validColNames() {
        initASheet();
        return new Object[][]{
                {RandomStringUtils.randomAscii(50), 5}, //gotta be a better way that using column number
                {RandomStringUtils.randomAscii(1), 6},
                {"आपल्याकडे स्ट्रिंग निर्माण करण्याचा चांगला मार्ग अ", 7},
                // i can't do the following cause chromeDriver only supports basic multilingual
                //{RandomStringUtils.random(50), 7},

                {RandomStringUtils.randomNumeric(50), 8}
        };

    }

    /**
     * Make sure added column shows up if using valid names
     * @param colName
     * @param colNumber
     */
    @Test(dataProvider = "validColNames")
    public void addColumn(String colName, int colNumber) {
        addColumnOnSheetPage(colName);


       // browser.findElement(By.xpath("//div[span[text()='OK']]"));
        WebElement addedColumn =
                browser.findElement(By.cssSelector(String.format(".clsGMO:nth-child(%d) > .clsTableHeadingText",colNumber)));
        assertThat(addedColumn.getText(), containsString(colName));
    }

    /**
     * Empty column names are allowed, they are simply named 'Column# by default'
     */
    @Test
    public void addEmptyNameColumn() {
        initASheet();
        addColumnOnSheetPage("");

        WebElement addedColumn =
                browser.findElement(By.cssSelector(String.format(".clsGMO:nth-child(%d) > .clsTableHeadingText",5)));
        assertThat(addedColumn.getText(), containsString("Column2"));
    }

    /**
     * Go through the workflow to add a column on the current sheet
     */
    private void addColumnOnSheetPage(String colName) {
        sleep(8);
        WebElement insertColButton = browser.findElement(By.cssSelector("*[data-client-id='tlb-80']"));
        insertColButton.click();
        sleep(3);

        WebElement colNameForm = browser.findElement(By.id("foid:29"));
        colNameForm.sendKeys(colName);colNameForm.sendKeys(colName);
        sleep(1);
        WebElement addcolButton = browser.findElement(By.xpath("//div[span[text()='OK']]"));
        addcolButton.click();
        sleep(2);
        Actions actions = new Actions(browser);
        actions.keyDown(Keys.CONTROL);
        actions.sendKeys("s").build().perform();
        sleep(2);

    }

}

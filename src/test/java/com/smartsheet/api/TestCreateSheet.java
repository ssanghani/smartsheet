package com.smartsheet.api;

import com.smartsheet.SpecFactory;
import com.smartsheet.TestProperties;
import com.smartsheet.api.Sheet;
import com.smartsheet.TestBase;
import com.smartsheet.json.ColumnCreateRequest;
import com.smartsheet.json.SheetCreateRequest;
import com.smartsheet.json.SheetCreateResponse;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.ValidatableResponse;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class TestCreateSheet extends TestBase {

    /**
     * Create a basic sheet, make sure the response code is 200 and
     * the returned fields look ok.
     */
    @Test
    public void testHappyPath() {
        Sheet s = new Sheet();
        ValidatableResponse vr = s.create();
        assertThat("sheet create failed",
                vr.extract().statusCode(), equalTo(200));

        checkResponseFields(s.getResponse());
        // clean up on success
        assertThat(s.delete().extract().statusCode(), equalTo(200));
    }


    @DataProvider
    public static Object[][] validSheetNames() {
        return new Object[][]{
                {RandomStringUtils.randomAscii(50)},
                {RandomStringUtils.randomAscii(1)},
                {RandomStringUtils.random(33)}, //unicode
                {RandomStringUtils.random(50)},
                {RandomStringUtils.randomNumeric(50)},


        };

    }

    /**
     * Create a bunch of sheets driven by the dataprovider to test
     * valid name values
     * @param name
     *      sheet name
     */
    @Test(dataProvider = "validSheetNames")
    public void testVariousSheetNames(String name) {
        ColumnCreateRequest c = new ColumnCreateRequest(true,
                RandomStringUtils.randomAlphanumeric(8), "TEXT_NUMBER");
        SheetCreateRequest sRequest = new SheetCreateRequest(name);
        sRequest.addColumn(c);

        Sheet sheet = new Sheet();
        ValidatableResponse vr = sheet.create(sRequest, SpecFactory.getApiSpec());
        assertThat("sheet create failed for " + name,
                vr.extract().statusCode(), equalTo(200));

        SheetCreateResponse sResponse = sheet.getResponse();
        checkResponseFields(sResponse);

        assertThat(sResponse.getResult().getName(), equalTo(name));

        // makes sure the sheet actually existed, also cleanup
        sheet.delete();
    }


    @DataProvider
    public static Object[][] inValidSheetNames() {
        String err1 = "The value for sheet.name must be 50 characters in length or less";
        String err2 = "Required object attribute(s) are missing from your request";
        return new Object[][]{
                {RandomStringUtils.randomAscii(51), 1041, err1},
                {RandomStringUtils.random(10000), 1041, err1},
                {RandomStringUtils.randomNumeric(51), 1041, err1},
                {RandomStringUtils.randomAscii(0), 1012, err2}

        };

    }


    /**
     * Create a bunch of sheets driven by the dataprovider to test
     * invalid name values
     * @param name
     *      sheet name
     */
    @Test(dataProvider = "inValidSheetNames")
    public void testVariousInvalidSheetNames(String name, int errorCode, String errorMsg) {
        ColumnCreateRequest c = new ColumnCreateRequest(true,
                RandomStringUtils.randomAlphanumeric(8), "TEXT_NUMBER");
        SheetCreateRequest sRequest = new SheetCreateRequest(name);
        sRequest.addColumn(c);

        Sheet sheet = new Sheet();
        ValidatableResponse vr = sheet.create(sRequest, SpecFactory.getApiSpec());
        assertThat("sheet got created when i expected it to fail " + name,
                vr.extract().statusCode(), equalTo(400));

        assertThat(vr.extract().body().asString(),
                containsString(String.valueOf(errorCode)));

        assertThat(vr.extract().body().asString(),
                containsString(errorMsg));

    }

    /**
     * Make sure you can't access api with the wrong key
     *
     */
    @Test
    public void testCreateSheetWithBadCreds() {
        ColumnCreateRequest c = new ColumnCreateRequest(true,
                RandomStringUtils.randomAlphanumeric(8), "TEXT_NUMBER");

        SheetCreateRequest s = new SheetCreateRequest(RandomStringUtils.randomAlphanumeric(8));
        s.addColumn(c);
        Sheet sheet = new Sheet();

        TestProperties prop = TestProperties.INSTANCE;
        // this should move to spec factory
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addHeader("Authorization", "badkey");
        builder.setContentType("application/json");
        builder.setBaseUri(prop.getApiEndpoint());

        sheet.create(s, builder.build()).statusCode(is(403));

    }

    /**
     * Try changing the content-type.
     * this is not a great test, cause encoding happens in the client.
     * Leaving it in just to be sure.
     */
    @Test
    public void testCreateSheetWithBadContentType() {
        ColumnCreateRequest c = new ColumnCreateRequest(true,
                RandomStringUtils.randomAlphanumeric(8), "TEXT_NUMBER");

        SheetCreateRequest s = new SheetCreateRequest(RandomStringUtils.randomAlphanumeric(8));
        s.addColumn(c);
        Sheet sheet = new Sheet();

        TestProperties prop = TestProperties.INSTANCE;
        // this should move to spec factory
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addHeader("Authorization", prop.getApiKey());
        builder.setContentType("text/html; charset=utf-8");
        builder.setBaseUri(prop.getApiEndpoint());

        sheet.create(s, builder.build()).statusCode(is(415));

    }

    //internal check if the response fields are valid
    private void checkResponseFields(SheetCreateResponse response) {
        assertThat(response.getMessage(), equalTo("SUCCESS"));
        assertThat(response.getResultCode(), equalTo(0));

        assertThat(response.getResult().getAccessLevel(), equalTo("OWNER"));

        assertThat(response.getResult().getColumns().size(), is(greaterThan(0)));

        // check atleast one column
        assertThat(response.getResult().getColumns().get(0).getTitle(), is(notNullValue()));
        assertThat(response.getResult().getColumns().get(0).getId(), is(notNullValue()));
        assertThat(response.getResult().getColumns().get(0).getType(), is(notNullValue()));
        // getSymbol on column is null...maybe put a default in there for all the optional fields
        assertThat(response.getResult().getPermalink(), is(notNullValue()));
        assertThat(response.getResult().getId(), is(notNullValue()));
        assertThat(response.getResult().getName(), is(notNullValue()));

    }
}

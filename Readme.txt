- I spent most of my time building the framework, cause I wanted easy/short tests
- src/main contains the framework pieces. src/test contains the tests.
- Ideally we'd have unit tests for things in src/main
- This took about 6 hours total spread over 2 sittings.


****************************************How to Run*****************************************
- First go to config/my.properties and set the right webPassword there (I can send you mine separately)
- If you are using your env, change the properties there to match.
- Install gradle
- On Mac - ./gradlew clean test

Alternatively for Intellij:
- Import the project
- I am using lombok. So if you are loading this in IntelliJ make sure annotation processors
  is enable in settings and you have the lombok plugin.
- Pick any test and run. If it fails go to your Run Config. Make sure the Run Directory is set
to something like '$MODULE_WORKING_DIR$'. Or just set it to the top level dir of smartsheet.

****************************************Framework Design*******************************************
- This is a pretty simple framework built on testng, rest assured and hamcrest.

- Properties are read in via the singleton (enum) TestProperties.

- For now I hardcoded the filename to be my.properties. This could just as easily be a soft link. However we want to
change the value via env variable as well, to integrate with CI.
- main/com.smartsheet.json - contains all the classes to serialize/deserialize Json requests and responses.
- main/com.smartsheet.api - contains classes to deal with the api endpoints so they can be reused between testcases.
- main/com.smartsheet.web - contains web, contains Selenium stuff.
- SpecFactory is our initial implementation of 'RequestSpecifications'. This is a rest-assured
concept. Think how will you easily talk to the front end vs an internal service. That can
 be easily done by extending SpecFactory.

- I wanted to build inheritance to push common things out of tests. I did this for the TestCreateColumn which inherits
 from TestSelenium. We might want to do similar things for the API tests.

****************** Tests ****************
1) Create Sheet API (TestCreateSheet)
- Obviously this will in reality be a lot bigger than what I implemented. I wanted to spend
 more time on framework than actual tests.
- Because I have json serializers/deserializers its easy enough in the test to check values.
- Most of my tests simply vary the Sheet name, API key and content-type.
- We do want a lot more tests around the Columns piece (eg max number of columns, 0 columns, special chars)
However I felt this would outside the scope of just Create Sheet.
- Based on the framework design, I'm pretty confident these tests can be run in parallel.


2) Create Column (TestCreateColumn)
- I am mostly checking that valid characters in the column name show up after its created.
- These tests are pretty basic. I'm clearly missing tests for 'right/left-click' to create a column. Also on the actual
Create Column popup, I probably should be making sure TEXT/NUMBER is actually selected and not default.



*********************** Future updates *********************
- I would redesign the Chrome/Browser classes (and add classes for other browsers). Chrome should probably extend
ChromeDriver instead. That way we can deal with differences between these drivers cleanly.
eg: ChromeDriver does not support emojis. Firefox Does. So we can have different generateRandomInput
 methods for Chrome and Firefox

- The sleeps in the Selenium test are *bad* and make for brittle tests. We should re-do that with polling to wait for
events instead.

- SeleniumTestBase should inherit from TestBase. And we should have an APITestBase do the same. This will allow for
common things (eg logging, setup) to live in TestBase.

- We need a SheetSelenium.java class much like the Sheet.java class. This might end up being multiple classes, just like
the API piece. This will allow us to save of id's and selectors in a single place and make them easier to update. Much
better than using them directly in tests.

